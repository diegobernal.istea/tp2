FROM centos:7
MAINTAINER The CentOS Project <cloud-ops@centos.org>
LABEL Vendor="CentOS" \
      License=GPLv2 \
      Version=2.4.6-40 \
      description="vortice"

#Seteamos los repositorios necesarios para contar con wsgi version 3+ de python
#Instalamos python, apache (en su version centOS)
#Instalamos el wsgi del repo ius
RUN yum -y --setopt=tsflags=nodocs install https://repo.ius.io/ius-release-el7.rpm && \
    yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install python36 && \
    yum -y --setopt=tsflags=nodocs install httpd && \
    yum -y --setopt=tsflags=nodocs install python36-mod_wsgi.x86_64 && \
    yum clean all

#Copiamos los requerimientos que, en resumen, son librerias que instalara PIP para que funcione nuestro microframework
#Corremos la instalacion de los requerimientos dentro del container sobre la imagen base definida
COPY requirements.txt /opt/app/requirements.txt
WORKDIR /opt/app
RUN pip3 install -r requirements.txt

#Seteamos el directorio de trabajo, aquel en el que vamos a estar parados en caso de hacer "exec bash" y que sera tomado como "raiz" para el 'COPY'
#Copiamos el archivo de la web server gateway inteface que nos permite utilizar python
#Copiamos el archivo que contiene las configuraciones necesarias para levantar apache con python
#Copiamos el archivo .py que sera (en terminos de analogia) nuestro index.php
#Copiamos al dirctorio de templates (aquel que usa el microframwork para levantar el html)
WORKDIR /var/www/html/FlaskApp/FlaskApp
COPY FlaskApp.wsgi /var/www/html/FlaskApp/FlaskApp.wsgi
COPY FlaskApp.conf /etc/httpd/conf.d/FlaskApp.conf
COPY __init__.py /var/www/html/FlaskApp/FlaskApp/__init__.py
COPY templates/index.html templates/index.html

#le decimos al container que trabaje en el puerto 80
EXPOSE 80

#En vez de entrypoint, usamos 'CMD' para inicializar el microservicio
CMD ["/usr/sbin/apachectl","-D","FOREGROUND"]

