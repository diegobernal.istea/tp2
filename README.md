 Trabajo Practico N2 Actualizacion Tecnologica - ISTEA

Editar el archivo host, ya sea un windows o linux en el equipo donde se ejecuta el navegador que testeara el desarrollo de manera tal que el dominio sqlflask.local resuelva la IP donde se aloja docker

Generamos una bridge network de docker. Esto permite que ambos containers puedan resolver su ip nateada a traves del hostname y ayuda a no hardcodear direcciones IP
```
docker network create -d bridge myNetwork
```

Creamos el container que hara de proxy reverso
```
docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro --name nproxy --network myNetwork jwilder/nginx-proxy
```

Iinicializar el container de la base de datos.
```
docker run --name sqlMySQL -p 3306:3306 -p 33060:33060 --network myNetwork -e MYSQL_DATABASE=tasks -e MYSQL_USER=diego -e MYSQL_PASSWORD=my-secret-pw -e MYSQL_ROOT_PASSWORD=my-secret-pw -d -v myVolumeSQL:/var/lib/mysql mysql:8
```

Definimos la tabla utilizando el usuario seteado via variables de entorno.
```
docker exec -it sqlMySQL mysql -u diego -p -e "USE tasks; CREATE TABLE IF NOT EXISTS task (id INT AUTO_INCREMENT PRIMARY KEY, content VARCHAR(200) NOT NULL, done BOOLEAN, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=INNODB;"
```

Construimos el container de front-end situandonos en la carpeta 'sqlFlask/'
```
docker build -t sqlflask/mysql .
```

Inicializamos el container del front-end
(No hace falta el flag para señalar puerto)
```
docker run -dit --name sqlflask -e VIRTUAL_HOST=sqlflask.local -p 8080:80 --network myNetwork sqlflask/mysql
```

#Por algun motivo (imagino que los pocos recursos asignados a la VM) al cargar la DB llamando al comando mysql dentro del container ocurre el siguiente error ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/run/mysqld/mysqld.sock' (2)


Optimizaciones:
	Utilizar dockercompose para facilitar el despliegue inicial
	Construir una nueva imagen a partir de la utilizada MySQL para crear la tabla en la DB a traves del entrypoint
	Mejorar los nombres de los contenedores
	Sumar markup al README.md



Informacion de httpd y Flask dentro del container:
```
[root@fee25e6d3c46 app]# tree /var/www/
/var/www/
|-- FlaskApp
|   `-- FlaskApp
|-- cgi-bin
`-- html
    `-- FlaskApp
        |-- FlaskApp
        |   `-- __init__.py
        `-- flaskapp.wsgi
```


```
[root@fee25e6d3c46 app] cat /etc/httpd/conf.d/FlaskApp.conf
<VirtualHost *:80>
                ServerName mywebsite.com
                ServerAdmin admin@mywebsite.com
                WSGIScriptAlias / /var/www/html/FlaskApp/flaskapp.wsgi
                <Directory /var/www/html/FlaskApp/FlaskApp/>
                        Order allow,deny
                        Allow from all
                </Directory>
                Alias /static /var/www/html/FlaskApp/FlaskApp/static
                <Directory /var/www/html/FlaskApp/FlaskApp/static/>
                        Order allow,deny
                        Allow from all
                </Directory>
                ErrorLog /var/log/httpd/error.log
                LogLevel warn
                CustomLog /var/log/httpd/access.log combined
</VirtualHost>
```

